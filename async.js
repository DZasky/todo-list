export function getTodoList(callback) {
    document.getElementById('list').innerHTML = ''; 
    fetch('http://195.181.210.249:3000/todo/').then((response) => {
        return response.json();
    }).then((response) => {
        callback(response);
    }).catch (() => {
        console.log('Error');
    });
};

export function createTodo(title, callback) {
    fetch('http://195.181.210.249:3000/todo', {
        method: "post",
        body: JSON.stringify({title: title}),
        headers: {
          'Content-Type': 'application/json',
        },
    }).then((response) => {
        return response.json();
    }).then(() => {
        getTodoList(callback);
    }).catch(() => {
        console.error('Error: POST!');
    });
};

export function deleteTodo(id, callback) {
    fetch(`http://195.181.210.249:3000/todo/${id}`, {
        method: "delete",
    }).then((response) => {
        return response.json();
    }).then(() => {
        getTodoList(callback);
    }).catch(() => {
        console.error('Error: DELETE!');
    });
};

export function editTodo(id, title, callback) {
    fetch(`http://195.181.210.249:3000/todo/${id}`, {
        method: "put",
        body: JSON.stringify({title: title}),
        headers: {
          'Content-Type': 'application/json',
        },
    }).then((response) => {
        return response.json();
    }).then(() => {
        getTodoList(callback);
    }).catch(() => {
        console.error('Error: PUT!');
    });
};