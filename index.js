import {getTodoList, createTodo, deleteTodo, editTodo} from './async.js'

let $list, form, btnAddTodo, todoInput, editInput, modal, closeModal, closeIcon, acceptTodo;
let currentId = 0;

function main() {
  prepareDOMElements();
  prepareDOMEvents();
  getTodoList(prepareInitialList);
};

function prepareDOMElements() {
  $list = document.getElementById('list');
  btnAddTodo = document.getElementById('addTodo');
  form = document.querySelector('.addForm');
  todoInput = document.querySelector('#myInput');
  editInput = document.querySelector('#popupInput');
  modal = document.querySelector('#myModal');
  closeModal = document.querySelector('#cancelTodo');
  closeIcon = document.querySelector('#closePopup');
  acceptTodo = document.querySelector('#acceptTodo');
};

function prepareDOMEvents() {
  $list.addEventListener('click', listClickManager);
  form.addEventListener('submit', function (event) {
    event.preventDefault();
      const inputValue = todoInput.value;
      if (inputValue.trim() !== '') {
        createTodo(inputValue, prepareInitialList);
        todoInput.value = '';
      };
  });

  closeModal.addEventListener('click', function() {
    editInput.value = '';
    closePopup();
  });
  
  closeIcon.addEventListener('click', function() {
    editInput.value = '';
    closePopup();
  });
  
  acceptTodo.addEventListener('click', function () {
    const inputValue = editInput.value;
    if (inputValue.trim() !== '') {
      editTodo(currentId, inputValue, function(response) {
        prepareInitialList(response);
        closePopup();
        editInput.value = '';
      });
    };
  });
};

function prepareInitialList(response) {
  response.forEach(todo => {
    addNewElementToList(todo.id, todo.title);
  });
};

function addNewElementToList(id, title) {
  const li = document.createElement('li');
  list.appendChild(li);
  const span = document.createElement('span');
  span.innerText = title;
  li.setAttribute('data-id', id);


  const deleteButton = document.createElement('button');
  deleteButton.setAttribute('class', 'deleteBtn'); 
  deleteButton.innerText = 'DELETE';
  const editButton = document.createElement('button');
  editButton.setAttribute('class' , 'editBtn');
  editButton.innerText = 'EDIT';
  const doneButton = document.createElement('button');
  doneButton.setAttribute('class', 'doneBtn');
  doneButton.innerText = 'DONE';

  li.appendChild(span);
  li.appendChild(doneButton);
  li.appendChild(editButton);
  li.appendChild(deleteButton);

  $list.appendChild(li);
};

function listClickManager(event) {
  if (event.target.className === 'editBtn') {
    currentId = event.target.parentElement.dataset.id;
    openPopup();
  } else if (event.target.className === 'deleteBtn') {
    deleteTodo (event.target.parentElement.dataset.id, prepareInitialList);
  } else if (event.target.className === 'doneBtn') {
    event.target.parentElement.classList.toggle('mark');
  };
};

function openPopup() {
  modal.style.display = 'block';
  const currentTodoText = document.querySelector(`[data-id = "${currentId}"]`).querySelector('span').innerText;
  editInput.value = currentTodoText;
};

function closePopup() {
  modal.style.display = 'none';
};

document.addEventListener('DOMContentLoaded', main);